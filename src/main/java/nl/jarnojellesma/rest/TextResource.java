package nl.jarnojellesma.rest;

import java.util.List;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.Produces;
import nl.jarnojellesma.text.WordFrequency;
import nl.jarnojellesma.text.WordFrequencyAnalyzer;
import nl.jarnojellesma.text.WordFrequencyAnalyzerImpl;

@Path("/text")
public class TextResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/highest-frequency")
    public int highestFrequency(@QueryParam("text") String text) {
        WordFrequencyAnalyzer analyzer = new WordFrequencyAnalyzerImpl();
        return analyzer.calculateHighestFrequency(text);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/word-frequency")
    public int wordFrequency(@QueryParam("text") String text, @QueryParam("word") String word) {
        WordFrequencyAnalyzer analyzer = new WordFrequencyAnalyzerImpl();
        return analyzer.calculateFrequencyForWord(text, word);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/most-frequent-words")
    public List<WordFrequency> mostFrequentNWords(@QueryParam("text") String text, @QueryParam("n") int n) {
        WordFrequencyAnalyzer analyzer = new WordFrequencyAnalyzerImpl();
        return analyzer.calculateMostFrequentNWords(text, n);
    }
}
