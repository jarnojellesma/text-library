package nl.jarnojellesma.text;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordFrequencyAnalyzerImpl implements WordFrequencyAnalyzer {
    public int calculateHighestFrequency(String text) {
        List<Integer> frequencies = calculateWordFrequencies(text).values().stream()
            .sorted(Comparator.reverseOrder())
            .toList();
        return frequencies.get(0);
    }

    public int calculateFrequencyForWord(String text, String word) {
        return calculateWordFrequencies(text).get(word);
    }

    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
        return calculateWordFrequencies(text).entrySet().stream()
            .map(e -> new WordFrequencyImpl(e.getKey(), e.getValue()))
            .sorted(Comparator.comparingInt(WordFrequencyImpl::getFrequency).reversed())
            .limit(n)
            .collect(Collectors.toList());
    }

    private Map<String, Integer> calculateWordFrequencies(String text) {
        return Arrays.stream(text.split(" "))
            .map(String::toLowerCase)
            .collect(Collectors.groupingBy(
                Function.identity(), 
                Collectors.collectingAndThen(Collectors.counting(), Long::intValue)
            ));
    }
}
