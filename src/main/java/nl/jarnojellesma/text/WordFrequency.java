package nl.jarnojellesma.text;

public interface WordFrequency {
    String getWord();
    int getFrequency();
}
