package nl.jarnojellesma.text;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class WordFrequencyAnalyzerImplTest {

    public String sampleText = "Hello this iS a sample text that is used during the tests that covers tHat that IS THE";

    @Test
    public void testCalculateFrequencyForWord() {
        WordFrequencyAnalyzerImpl wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
        int frequency = wordFrequencyAnalyzer.calculateFrequencyForWord(sampleText, "is");
        Assert.assertEquals(3, frequency);
    }

    @Test
    public void testCalculateHighestFrequency() {
        WordFrequencyAnalyzerImpl wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
        int frequency = wordFrequencyAnalyzer.calculateHighestFrequency(sampleText);
        Assert.assertEquals(4, frequency);
    }

    @Test
    public void testCalculateMostFrequentNWords() {
        WordFrequencyAnalyzerImpl wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
        List<WordFrequency> wordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentNWords(sampleText, 3);
        
        Assert.assertEquals(3, wordFrequencies.size());

        Assert.assertEquals(4, wordFrequencies.get(0).getFrequency());
        Assert.assertEquals("that", wordFrequencies.get(0).getWord());

        Assert.assertEquals(3, wordFrequencies.get(1).getFrequency());
        Assert.assertEquals("is", wordFrequencies.get(1).getWord());

        Assert.assertEquals(2, wordFrequencies.get(2).getFrequency());
        Assert.assertEquals("the", wordFrequencies.get(2).getWord());
    }
}
